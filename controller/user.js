
const database = require('../config/database');

class Users {
    static getUsers() {
        return new Promise((resolve, reject) => {
            database.getConnection(function (err, connection) {
                if (err) throw reject(err);

                connection.query("select * from Users", function (error, results, fields) {
                    if (error) reject(error);
                    resolve(results);
                    connection.release();
                });
            });
        });
    }

    static getUser(id) {
        return new Promise((resolve, reject) => {
            database.getConnection(function (err, connection) {
                if (err) throw reject(err);

                connection.query(`select * from Users where id=${id};`, function (error, results, fields) {
                    if (error) throw reject(error);

                    resolve(results[0]);
                    connection.release();
                });
            });
        });
    }

    static saveUser(user) {
        return new Promise((resolve, reject) => {
            database.getConnection(function (err, connection) {
                if (err) throw reject(err);

                connection.query(`insert into Users values (DEFAULT, '${user.name}', '${user.email}', '${user.password}');`, function (error, results, fields) {
                    if (error) throw reject(error);
                    resolve(results.affectedRows);
                    connection.release();
                });
            });
        });
    }

    static updateUser(id, user) {
        return new Promise((resolve, reject) => {
            database.getConnection(function (err, connection) {
                if (err) throw reject(err);

                connection.query(`update Users set name='${user.name}', email='${user.email}', password='${user.password}' where id=${id};`, function(error, results, fields) {
                    if (error) throw reject(error);
                    resolve(results.affectedRows);
                    connection.release();
                });
            });
        });
    }

    static removeUser(id) {
        return new Promise((resolve, reject) => {
            database.getConnection(function (err, connection) {
                if (err) throw reject(error);

                connection.query(`delete from Users where id=${id};`, function (error, results, fields) {
                    if (error) throw reject(error);
                    resolve(results.affectedRows);
                    connection.release();
                });
            });
        });
    }
};

module.exports = { Users };