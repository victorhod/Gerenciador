
// USER METHODS

function remove(user) {
    $.ajax({
        url: '/user/remove/' + $(user).attr('data-id'),
        method: 'DELETE',
        success: function (result) {
            location.href = '/';
            console.log('Sucessful Removed');
        },
        error: function (error) {
            console.log('Error Removed');
        }
    })
}

function select(user) {
    $.ajax({
        url: '/user/' + $(user).attr('data-id'),
        method: 'GET',
        success: function (result) {
            $('#userid').val(result.id);
            $('#username').val(result.name);
            $('#useremail').val(result.email);
            $('#userpassword').val(result.password);
        },
        error: function (error) {
            console.log('Error to get user');
        }
    })
}

function update(id) {
    $.ajax({
        url: '/user/' + id,
        method: "PATCH",
        data: {
            name: $('#username').val(),
            email: $('#useremail').val(),
            password: $('#userpassword').val()
        },
        success: function(result) {
            location.href = '/';
        },
        error: function(error) {
            console.log('error to update user');
        }
    });
}

// FORM

$('form').submit( function(e) {
    e.preventDefault();
    if ($('#userid').val() === '') {
        $.ajax({
            url: '/user',
            method: 'POST',
            data: {
                name: $('#username').val(),
                email: $('#useremail').val(),
                password: $('#userpassword').val()
            },
            success: function(result) {
                location.href = '/';
            }
        });
    } else {
        update($('#userid').val());
    }
});