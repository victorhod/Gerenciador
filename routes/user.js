
const Routes = require('express').Router();
const {Users} = require('../controller/user');
const User = require('../model/user');

Routes.get('/', async (req, res) => {
    try {
        res.render('index', {
            users: await Users.getUsers(),
            Users: Users
        });
    } catch(e) {
        console.log(e);
    }
});

Routes.get('/user/:id', async (req, res) => {
    try {
        let id = req.params.id;
        let user = await Users.getUser(id);
        if(user) {
            res.send(user);
        }
    } catch(e) {
        console.log(e);
    }
});

Routes.post('/user', async (req, res) => {
    try {
        let body = req.body;
        let user = new User(body.name, body.email, body.password);
        let affectedRows = await Users.saveUser(user);
        if (affectedRows > 0) {
            res.render('index', {
                users: await Users.getUsers()
            });
        } else {
            res.send();
        }
    } catch(e) {
        console.log(e);
    }
});

Routes.delete('/user/remove/:id', async (req, res) => {
    try {
        let id = req.params.id;
        let affectedRows = await Users.removeUser(id);
        if (affectedRows > 0) {
            res.render('index', {
                users: await Users.getUsers()
            });
        } else {
            res.send();
        }
    } catch(e) {
        console.log(e);
    }
});

Routes.patch('/user/:id', async (req, res) => {
    try {
        let id = req.params.id;
        let body = req.body;
        let user = new User(body.name, body.email, body.password);
        let affectedRows = await Users.updateUser(id, user);
        if (affectedRows > 0) {
            res.render('index', {
                users: await Users.getUsers()
            });
        } else {
            res.send();
        }
    } catch(e) {
        console.log(e);
    }
});

module.exports = { Routes };