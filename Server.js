//
//
//
// Variables and Constants
const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = process.env.PORT || 3000;
const { Routes } = require('./routes/user');

 // Middlewares
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(express.static('views'));
app.set('view engine', 'ejs');

// Routes
app.use('/', Routes);

// Server
app.listen(port, () => {
    console.log(`Server listen on ${port}`);
});

module.exports = {app};